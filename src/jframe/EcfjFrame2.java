/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jframe;

import classe.metier.Client;
import classe.metier.Prospect;
import classe.metier.Societe;
import classe.metier.Societe.DomaineSociete_Enum;
import jframe.EcfJFrame1.choixFormulaire;
import exception.metier.personnalise.AttributSocieteInvalide;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *Fenetre pour afficher le formulaire de création, modification et suppression
 * @author cda 24
 */
public class EcfjFrame2 extends javax.swing.JFrame {
    
    // Attribut d'instance------------------------------------------------------

    private Client clientModifSuppr;        // Objet client utilisé pour la modification ou la supression
    private Prospect prospectModifSuppr;    // Objet prospect utilisé pour la modification ou la supression

    
    // Constructeur------------------------------------------------------------- 

    /**
     * Constructeur avec choix formulaire création
     * @param choix type int
     */
    public EcfjFrame2(choixFormulaire choix) {
        initComponents();
        
       
        switch(choix){
            
            case CREATION_CLIENT:
                initFormulaireCreationClients();
                break;
                
            case CREATION_PROSPECT:
                initFormulaireCreationProspect();
                break;
        }
        
    }
    
    /**
     * Constructeur avec choix formulaire suppression ou modification
     * @param choixFormulaire type int
     * @param societe type Client ou Prospect
     */
    public EcfjFrame2(choixFormulaire choixFormulaire, Societe societe) {
        initComponents();

        Client client = new Client();
        Prospect prospect = new Prospect();
        
        if (societe instanceof Client){
            client = (Client) societe;
        }
        else{
            prospect = (Prospect) societe;
        }
        
        switch(choixFormulaire){
            
            case MODIFICATION_CLIENT:
                initFenetre2ModificationClient(client); 
                break;
            case SUPPRESSION_CLIENT:
                initFenetre2SuppressionClient(client);
                break;
            case MODIFICATION_PROSPECT:
                initFenetre2ModificationProspect(prospect);
                break;
                
            case SUPPRESSION_PROSPECT:
                initFenetre2SuppressionProspect(prospect);
                break;
        }
        
    }

    private EcfjFrame2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    // Methode d'instance-------------------------------------------------------
    
    protected void initFormulaireCreationClients(){
        
        // Les Labels
         labelId2.setVisible(false);
        
        // Les Panels
        panelVariableProspect2.setVisible(false);
        
        // Les Texts 
        textFieldId2.setVisible(false);
        
        // Les bouttons
        buttonValide2.setText("Création Client");
        textFieldDomaine2.setVisible(false);
        
    }
    
    protected void initFormulaireCreationProspect(){
        
        // Les Labels
        labelTitre2.setVisible(true);
        labelTitre2.setText("Prospect");
        labelId2.setVisible(false);
        
        // Les Panels
        panelVariableClients2.setVisible(false);
        
        // Les Texts
        textFieldId2.setVisible(false);
        textFieldDomaine2.setVisible(false);
       
    }
    
     protected void initFormulaireModificationProspect(){
        
        // Les Labels
        labelTitre2.setVisible(false);
        labelSousTitre2.setVisible(false);
        
        // Les Panels
        panelVariableClients2.setVisible(false);
        
    }

    protected void initFenetre2ModificationClient(Client client){
        
        clientModifSuppr = client;
        labelSousTitre2.setText("Modification");
        panelVariableProspect2.setVisible(false);
        textFieldDomaine2.setVisible(false);
        textFieldId2.setText(Integer.toString(client.getIdSociete()));
        textFieldId2.setEditable(false);
        textFieldRaisonSocial2.setText(client.getRaisonSocialeSociete());
        comboBoxDomaine2.setSelectedItem(client.getDomaineSociete());
        textFieldNumeroRue2.setText(Integer.toString(client.getNumeroRueSociete()));
        textFieldNomRue2.setText(client.getNomRueSociete());
        textFieldCodePostal2.setText(client.getCodePostalSociete());
        textFieldVille2.setText(client.getVilleSociete());
        textFieldTelephone2.setText(client.getTelephoneSociete());
        textFieldMail2.setText(client.getMailSociete());
        textAreaCommentaire2.setText(client.getCommentairesSociete());
        textFieldChiffreDaffaire2.setText(Integer.toString(client.getChiffreDaffaireClient()));
        textFieldNombreDemployes2.setText(Integer.toString(client.getNombreEmployesClient()));
   
    }
    protected void initFenetre2SuppressionClient(Client client){
        
        clientModifSuppr = client;
        labelSousTitre2.setText("Suppression");
        panelVariableProspect2.setVisible(false);
        textFieldDomaine2.setVisible(false);
        textFieldId2.setText(Integer.toString(client.getIdSociete()));
        textFieldId2.setEditable(false);
        textFieldRaisonSocial2.setText(client.getRaisonSocialeSociete());
        textFieldRaisonSocial2.setEditable(false);
        comboBoxDomaine2.setSelectedItem(client.getDomaineSociete());
        comboBoxDomaine2.setEnabled(false);
        textFieldNumeroRue2.setText(Integer.toString(client.getNumeroRueSociete()));
        textFieldNumeroRue2.setEditable(false);
        textFieldNomRue2.setText(client.getNomRueSociete());
        textFieldNomRue2.setEditable(false);
        textFieldCodePostal2.setText(client.getCodePostalSociete());
        textFieldCodePostal2.setEditable(false);
        textFieldVille2.setText(client.getVilleSociete());
        textFieldVille2.setEditable(false);
        textFieldTelephone2.setText(client.getTelephoneSociete());
        textFieldTelephone2.setEditable(false);
        textFieldMail2.setText(client.getMailSociete());
        textFieldMail2.setEditable(false);
        textAreaCommentaire2.setText(client.getCommentairesSociete());
        textAreaCommentaire2.setEditable(false);
        textFieldChiffreDaffaire2.setText(Integer.toString(client.getChiffreDaffaireClient()));
        textFieldChiffreDaffaire2.setEditable(false);
        textFieldNombreDemployes2.setText(Integer.toString(client.getNombreEmployesClient()));
        textFieldNombreDemployes2.setEditable(false);

    }
    protected void initFenetre2ModificationProspect(Prospect prospect){
        
        prospectModifSuppr = prospect;
        labelTitre2.setText("Prospect");
        labelSousTitre2.setText("Modification");
        panelVariableClients2.setVisible(false);
        textFieldDomaine2.setVisible(false);     
        textFieldId2.setText(Integer.toString(prospect.getIdSociete()));
        textFieldId2.setEditable(false);
        textFieldRaisonSocial2.setText(prospect.getRaisonSocialeSociete());
        comboBoxDomaine2.setSelectedItem(prospect.getDomaineSociete());
        textFieldNumeroRue2.setText(Integer.toString(prospect.getNumeroRueSociete()));
        textFieldNomRue2.setText(prospect.getNomRueSociete());
        textFieldCodePostal2.setText(prospect.getCodePostalSociete());
        textFieldVille2.setText(prospect.getVilleSociete());
        textFieldTelephone2.setText(prospect.getTelephoneSociete());
        textFieldMail2.setText(prospect.getMailSociete());
        textAreaCommentaire2.setText(prospect.getCommentairesSociete());
        textFieldDateProspection2.setText(prospect.getDateProspectionProspect());
        comboBoxProspectionInteresse2.setSelectedItem(prospect.getProspectInteresseProspect());
        
    }
    protected void initFenetre2SuppressionProspect(Prospect prospect){
        
        prospectModifSuppr = prospect;
        labelTitre2.setText("Prospect");
        labelSousTitre2.setText("Suppression");
        panelVariableClients2.setVisible(false);      
        textFieldDomaine2.setVisible(false); 
        textFieldId2.setText(Integer.toString(prospect.getIdSociete()));
        textFieldId2.setEditable(false);
        textFieldRaisonSocial2.setText(prospect.getRaisonSocialeSociete());
        textFieldRaisonSocial2.setEditable(false);
        comboBoxDomaine2.setSelectedItem(prospect.getDomaineSociete());
        comboBoxDomaine2.setEnabled(false);
        textFieldNumeroRue2.setText(Integer.toString(prospect.getNumeroRueSociete()));
        textFieldNumeroRue2.setEditable(false);
        textFieldNomRue2.setText(prospect.getNomRueSociete());
        textFieldNomRue2.setEditable(false);
        textFieldCodePostal2.setText(prospect.getCodePostalSociete());
        textFieldCodePostal2.setEditable(false);
        textFieldVille2.setText(prospect.getVilleSociete());
        textFieldVille2.setEditable(false);
        textFieldTelephone2.setText(prospect.getTelephoneSociete());
        textFieldTelephone2.setEditable(false);
        textFieldMail2.setText(prospect.getMailSociete());
        textFieldMail2.setEditable(false);
        textAreaCommentaire2.setText(prospect.getCommentairesSociete());
        textAreaCommentaire2.setEditable(false);
        textFieldDateProspection2.setText(prospect.getDateProspectionProspect());
        textFieldDateProspection2.setEditable(false);
        comboBoxProspectionInteresse2.setSelectedItem(prospect.getProspectInteresseProspect());
        comboBoxProspectionInteresse2.setEnabled(false);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelTitre2 = new javax.swing.JLabel();
        panelVariableSociete2 = new javax.swing.JPanel();
        textFieldNumeroRue2 = new javax.swing.JTextField();
        textFieldNomRue2 = new javax.swing.JTextField();
        textFieldCodePostal2 = new javax.swing.JTextField();
        textFieldVille2 = new javax.swing.JTextField();
        textFieldTelephone2 = new javax.swing.JTextField();
        textFieldMail2 = new javax.swing.JTextField();
        labelId2 = new javax.swing.JLabel();
        labelRaisonSocial2 = new javax.swing.JLabel();
        labelDomaine2 = new javax.swing.JLabel();
        labelAdresse2 = new javax.swing.JLabel();
        labelNumeroRue2 = new javax.swing.JLabel();
        labelSousTitre2 = new javax.swing.JLabel();
        labelNomRue2 = new javax.swing.JLabel();
        labelCodePostal2 = new javax.swing.JLabel();
        labelVille2 = new javax.swing.JLabel();
        labelTelephone2 = new javax.swing.JLabel();
        labelMail2 = new javax.swing.JLabel();
        textFieldId2 = new javax.swing.JTextField();
        textFieldRaisonSocial2 = new javax.swing.JTextField();
        labelCommentaire2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        textAreaCommentaire2 = new javax.swing.JTextArea();
        comboBoxDomaine2 = new javax.swing.JComboBox<>();
        textFieldDomaine2 = new javax.swing.JTextField();
        panelVariableClients2 = new javax.swing.JPanel();
        textFieldChiffreDaffaire2 = new javax.swing.JTextField();
        textFieldNombreDemployes2 = new javax.swing.JTextField();
        labelChiffreDaffaire2 = new javax.swing.JLabel();
        labelNombreDemployes2 = new javax.swing.JLabel();
        panelVariableProspect2 = new javax.swing.JPanel();
        textFieldDateProspection2 = new javax.swing.JTextField();
        labelDateProspection2 = new javax.swing.JLabel();
        labelProspectionInteresse2 = new javax.swing.JLabel();
        comboBoxProspectionInteresse2 = new javax.swing.JComboBox<>();
        buttonQuitter2 = new javax.swing.JButton();
        buttonRetourAcceuil2 = new javax.swing.JButton();
        buttonValide2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelTitre2.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        labelTitre2.setText("Clients");

        labelId2.setText("ID :");

        labelRaisonSocial2.setText("Raison Social :");

        labelDomaine2.setText("Domaine :");

        labelAdresse2.setText("Adresse :");

        labelNumeroRue2.setText("Numéro de la rue :");

        labelSousTitre2.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        labelSousTitre2.setText("Création");

        labelNomRue2.setText("Nom de la rue :");

        labelCodePostal2.setText("Code Postal :");

        labelVille2.setText("Ville :");

        labelTelephone2.setText("Telephone :");

        labelMail2.setText("Mail de contact :");

        textFieldId2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldId2ActionPerformed(evt);
            }
        });

        textFieldRaisonSocial2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldRaisonSocial2ActionPerformed(evt);
            }
        });

        labelCommentaire2.setText("Commentaire : ");

        textAreaCommentaire2.setColumns(20);
        textAreaCommentaire2.setRows(5);
        jScrollPane1.setViewportView(textAreaCommentaire2);

        comboBoxDomaine2.setModel(new DefaultComboBoxModel(DomaineSociete_Enum.values())
        );
        comboBoxDomaine2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxDomaine2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelVariableSociete2Layout = new javax.swing.GroupLayout(panelVariableSociete2);
        panelVariableSociete2.setLayout(panelVariableSociete2Layout);
        panelVariableSociete2Layout.setHorizontalGroup(
            panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVariableSociete2Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelVariableSociete2Layout.createSequentialGroup()
                        .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelNumeroRue2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelNomRue2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelCodePostal2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelTelephone2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelMail2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelVille2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelCommentaire2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(textFieldNumeroRue2, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                            .addComponent(textFieldNomRue2, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                            .addComponent(textFieldCodePostal2, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                            .addComponent(textFieldVille2, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                            .addComponent(textFieldTelephone2, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                            .addComponent(textFieldMail2, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                            .addComponent(jScrollPane1)))
                    .addGroup(panelVariableSociete2Layout.createSequentialGroup()
                        .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelId2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelRaisonSocial2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelDomaine2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelAdresse2, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(textFieldId2, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textFieldRaisonSocial2, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelSousTitre2, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelVariableSociete2Layout.createSequentialGroup()
                                .addComponent(textFieldDomaine2, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboBoxDomaine2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelVariableSociete2Layout.setVerticalGroup(
            panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelVariableSociete2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelSousTitre2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldId2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelId2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelRaisonSocial2)
                    .addComponent(textFieldRaisonSocial2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelDomaine2)
                    .addComponent(comboBoxDomaine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldDomaine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelAdresse2)
                .addGap(18, 18, 18)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldNumeroRue2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelNumeroRue2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldNomRue2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelNomRue2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldCodePostal2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCodePostal2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldVille2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelVille2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldTelephone2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTelephone2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldMail2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelMail2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableSociete2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelCommentaire2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        labelChiffreDaffaire2.setText("Chiffre d'affaire :");

        labelNombreDemployes2.setText("Nombre d'employés");

        javax.swing.GroupLayout panelVariableClients2Layout = new javax.swing.GroupLayout(panelVariableClients2);
        panelVariableClients2.setLayout(panelVariableClients2Layout);
        panelVariableClients2Layout.setHorizontalGroup(
            panelVariableClients2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelVariableClients2Layout.createSequentialGroup()
                .addContainerGap(29, Short.MAX_VALUE)
                .addGroup(panelVariableClients2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelChiffreDaffaire2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelNombreDemployes2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableClients2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldChiffreDaffaire2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldNombreDemployes2, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        panelVariableClients2Layout.setVerticalGroup(
            panelVariableClients2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVariableClients2Layout.createSequentialGroup()
                .addGroup(panelVariableClients2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldChiffreDaffaire2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelChiffreDaffaire2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableClients2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldNombreDemployes2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelNombreDemployes2)))
        );

        labelDateProspection2.setText("Date de prospection :");

        labelProspectionInteresse2.setText("Prospection interessé :");

        comboBoxProspectionInteresse2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "OUI", "NON" }));
        comboBoxProspectionInteresse2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxProspectionInteresse2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelVariableProspect2Layout = new javax.swing.GroupLayout(panelVariableProspect2);
        panelVariableProspect2.setLayout(panelVariableProspect2Layout);
        panelVariableProspect2Layout.setHorizontalGroup(
            panelVariableProspect2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVariableProspect2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelVariableProspect2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelDateProspection2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelProspectionInteresse2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableProspect2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(textFieldDateProspection2)
                    .addComponent(comboBoxProspectionInteresse2, 0, 359, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelVariableProspect2Layout.setVerticalGroup(
            panelVariableProspect2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVariableProspect2Layout.createSequentialGroup()
                .addGroup(panelVariableProspect2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldDateProspection2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDateProspection2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelVariableProspect2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelProspectionInteresse2)
                    .addComponent(comboBoxProspectionInteresse2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonQuitter2.setText("Quitter");
        buttonQuitter2.setMaximumSize(new java.awt.Dimension(113, 32));
        buttonQuitter2.setMinimumSize(new java.awt.Dimension(113, 32));
        buttonQuitter2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonQuitter2ActionPerformed(evt);
            }
        });

        buttonRetourAcceuil2.setText("Retour Acceuil");
        buttonRetourAcceuil2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRetourAcceuil2ActionPerformed(evt);
            }
        });

        buttonValide2.setText("Valider");
        buttonValide2.setMaximumSize(new java.awt.Dimension(113, 32));
        buttonValide2.setMinimumSize(new java.awt.Dimension(113, 32));
        buttonValide2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonValide2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(370, 370, 370)
                        .addComponent(labelTitre2, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panelVariableSociete2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(728, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(panelVariableProspect2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelVariableClients2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(buttonValide2, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonQuitter2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonRetourAcceuil2, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(738, 738, 738))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelTitre2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(panelVariableSociete2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelVariableClients2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelVariableProspect2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonRetourAcceuil2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonQuitter2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonValide2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void textFieldRaisonSocial2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldRaisonSocial2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldRaisonSocial2ActionPerformed

    private void comboBoxProspectionInteresse2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxProspectionInteresse2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxProspectionInteresse2ActionPerformed

    private void comboBoxDomaine2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxDomaine2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxDomaine2ActionPerformed

    private void buttonQuitter2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonQuitter2ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_buttonQuitter2ActionPerformed

    
    
    
    
    private void buttonRetourAcceuil2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRetourAcceuil2ActionPerformed

        EcfJFrame1 ecfJFrame1 = new EcfJFrame1();
        ecfJFrame1.setTitle("Fenetre1");
        ecfJFrame1.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_buttonRetourAcceuil2ActionPerformed

    private void buttonValide2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonValide2ActionPerformed
            
                try {
                    if(labelTitre2.getText().equals("Clients") && labelSousTitre2.getText().equals("Création")){
                       
                        Client client = new Client();
                        client.setRaisonSocialeSociete(textFieldRaisonSocial2.getText());
                        client.setDomaineSociete(DomaineSociete_Enum.valueOf(comboBoxDomaine2.getSelectedItem().toString()));
                        client.setNumeroRueSociete(Integer.parseInt(textFieldNumeroRue2.getText()));
                        client.setNomRueSociete(textFieldNomRue2.getText());
                        client.setCodePostalSociete(textFieldCodePostal2.getText());
                        client.setVilleSociete(textFieldVille2.getText());
                        client.setTelephoneSociete(textFieldTelephone2.getText());
                        client.setMailSociete(textFieldMail2.getText());
                        client.setCommentairesSociete(textAreaCommentaire2.getText());
                        client.verificationChiffreDaffaireVsNombreDemployé(Integer.parseInt(textFieldChiffreDaffaire2.getText()), Integer.parseInt(textFieldNombreDemployes2.getText()));
                        client.setChiffreDaffaireClient(Integer.parseInt(textFieldChiffreDaffaire2.getText()));
                        client.setNombreEmployesClient(Integer.parseInt(textFieldNombreDemployes2.getText()));
                        client.setIdSociete(Client.getCompteurIdSociete());
                        Client.getListeClient().add(client);
                        
                        JOptionPane.showMessageDialog(null,"Féliciation votre client est Créer", "Client", JOptionPane.INFORMATION_MESSAGE);
                        
                        textFieldRaisonSocial2.setText("");
                        textFieldNumeroRue2.setText("");
                        textFieldNomRue2.setText("");
                        textFieldCodePostal2.setText("");
                        textFieldVille2.setText("");
                        textFieldTelephone2.setText("");
                        textFieldMail2.setText("");
                        textAreaCommentaire2.setText("");
                        textFieldChiffreDaffaire2.setText("");                       
                        textFieldNombreDemployes2.setText("");
                 
                    }
                    else if(labelTitre2.getText().equals("Prospect") && labelSousTitre2.getText().equals("Création")){
                        
                        Prospect prospect = new Prospect();
                        prospect.setRaisonSocialeSociete(textFieldRaisonSocial2.getText());
                        prospect.setDomaineSociete(DomaineSociete_Enum.valueOf(comboBoxDomaine2.getSelectedItem().toString()));
                        prospect.setNumeroRueSociete(Integer.parseInt(textFieldNumeroRue2.getText()));
                        prospect.setNomRueSociete(textFieldNomRue2.getText());
                        prospect.setCodePostalSociete(textFieldCodePostal2.getText());
                        prospect.setVilleSociete(textFieldVille2.getText());
                        prospect.setTelephoneSociete(textFieldTelephone2.getText());
                        prospect.setMailSociete(textFieldMail2.getText());
                        prospect.setCommentairesSociete(textAreaCommentaire2.getText());
                        prospect.setDateProspectionProspect((textFieldDateProspection2.getText()));
                        prospect.setProspectInteresseProspect(comboBoxProspectionInteresse2.getSelectedItem().toString());
                        prospect.setIdSociete(Client.getCompteurIdSociete());
                        Prospect.getListeProspect().add(prospect);
                        
                        JOptionPane.showMessageDialog(null,"Féliciation votre prospect est Créer", "Prospect", JOptionPane.INFORMATION_MESSAGE);
                        
                        textFieldRaisonSocial2.setText("");
                        textFieldNumeroRue2.setText("");
                        textFieldNomRue2.setText("");
                        textFieldCodePostal2.setText("");
                        textFieldVille2.setText("");
                        textFieldTelephone2.setText("");
                        textFieldMail2.setText("");
                        textAreaCommentaire2.setText("");
                        textFieldDateProspection2.setText("");                       
                        
                    }
                    else if(labelTitre2.getText().equals("Clients") && labelSousTitre2.getText().equals("Modification")){
                        
                        clientModifSuppr.setRaisonSocialeSociete(textFieldRaisonSocial2.getText());
                        clientModifSuppr.setDomaineSociete(DomaineSociete_Enum.valueOf(comboBoxDomaine2.getSelectedItem().toString()));
                        clientModifSuppr.setNumeroRueSociete(Integer.parseInt(textFieldNumeroRue2.getText()));
                        clientModifSuppr.setNomRueSociete(textFieldNomRue2.getText());
                        clientModifSuppr.setCodePostalSociete(textFieldCodePostal2.getText());
                        clientModifSuppr.setVilleSociete(textFieldVille2.getText());
                        clientModifSuppr.setTelephoneSociete(textFieldTelephone2.getText());
                        clientModifSuppr.setMailSociete(textFieldMail2.getText());
                        clientModifSuppr.setCommentairesSociete(textAreaCommentaire2.getText());
                        clientModifSuppr.verificationChiffreDaffaireVsNombreDemployé(Integer.parseInt(textFieldChiffreDaffaire2.getText()), Integer.parseInt(textFieldNombreDemployes2.getText()));
                        clientModifSuppr.setChiffreDaffaireClient(Integer.parseInt(textFieldChiffreDaffaire2.getText()));
                        clientModifSuppr.setNombreEmployesClient(Integer.parseInt(textFieldNombreDemployes2.getText()));
                        
                        JOptionPane.showMessageDialog(null,"Féliciation votre client est Modifié", "Modification", JOptionPane.INFORMATION_MESSAGE);
                        
                        EcfJFrame1 ecfJFrame1 = new EcfJFrame1();
                        ecfJFrame1.setTitle("Fenetre1");
                        ecfJFrame1.setVisible(true);
                        dispose();
                        
                    }
                    else if(labelTitre2.getText().equals("Prospect") && labelSousTitre2.getText().equals("Modification")){
                        
                        prospectModifSuppr.setRaisonSocialeSociete(textFieldRaisonSocial2.getText());
                        prospectModifSuppr.setDomaineSociete(DomaineSociete_Enum.valueOf(comboBoxDomaine2.getSelectedItem().toString()));
                        prospectModifSuppr.setNumeroRueSociete(Integer.parseInt(textFieldNumeroRue2.getText()));
                        prospectModifSuppr.setNomRueSociete(textFieldNomRue2.getText());
                        prospectModifSuppr.setCodePostalSociete(textFieldCodePostal2.getText());
                        prospectModifSuppr.setVilleSociete(textFieldVille2.getText());
                        prospectModifSuppr.setTelephoneSociete(textFieldTelephone2.getText());
                        prospectModifSuppr.setMailSociete(textFieldMail2.getText());
                        prospectModifSuppr.setCommentairesSociete(textAreaCommentaire2.getText());
                        prospectModifSuppr.setDateProspectionProspect(textFieldDateProspection2.getText());
                        prospectModifSuppr.setProspectInteresseProspect(comboBoxProspectionInteresse2.getSelectedItem().toString());
                        
                        JOptionPane.showMessageDialog(null,"Féliciation votre client est Modifié", "Modification", JOptionPane.INFORMATION_MESSAGE);
                        
                        EcfJFrame1 ecfJFrame1 = new EcfJFrame1();
                        ecfJFrame1.setTitle("Fenetre1");
                        ecfJFrame1.setVisible(true);
                        dispose();
                        
                    }
                    else if(labelTitre2.getText().equals("Clients") && labelSousTitre2.getText().equals("Suppression")){
                        
                        int input = JOptionPane.showConfirmDialog(null,"Souhaitez vous vraiment supprimer ce client ?", "Confirmation",JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE);
                        
                        if (input == 1){
                            JOptionPane.showMessageDialog(null,"Votre Client n'a pas était supprimé", "Suppression", JOptionPane.INFORMATION_MESSAGE);
                            EcfJFrame1 ecfJFrame1 = new EcfJFrame1();
                            ecfJFrame1.setTitle("Fenetre1");
                            ecfJFrame1.setVisible(true);
                            dispose();    
                        }
                        else{
                            Client.getListeClient().remove(clientModifSuppr);
                            JOptionPane.showMessageDialog(null,"Féliciation votre client est supprimé", "Suppression", JOptionPane.INFORMATION_MESSAGE);
                            EcfJFrame1 ecfJFrame1 = new EcfJFrame1();
                            ecfJFrame1.setTitle("Fenetre1");
                            ecfJFrame1.setVisible(true);
                            dispose();
                        }
                    }
                    else if(labelTitre2.getText().equals("Prospect") && labelSousTitre2.getText().equals("Suppression")){
                        
                        int input = JOptionPane.showConfirmDialog(null,"Souhaitez vous vraiment supprimer ce prospect?", "Confirmation",JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE);
                        
                        if (input == 1){
                            JOptionPane.showMessageDialog(null,"Votre Client n'a pas était supprimé", "Suppression", JOptionPane.INFORMATION_MESSAGE);
                            EcfJFrame1 ecfJFrame1 = new EcfJFrame1();
                            ecfJFrame1.setTitle("Fenetre1");
                            ecfJFrame1.setVisible(true);
                            dispose();    
                        }
                        else{
                            Prospect.getListeProspect().remove(prospectModifSuppr);
                            JOptionPane.showMessageDialog(null,"Féliciation votre prospect est supprimé", "Suppression", JOptionPane.INFORMATION_MESSAGE);
                            EcfJFrame1 ecfJFrame1 = new EcfJFrame1();
                            ecfJFrame1.setTitle("Fenetre1");
                            ecfJFrame1.setVisible(true);
                            dispose();
                        }
                    }
                } 
                catch (AttributSocieteInvalide ex) {
                    JOptionPane.showMessageDialog(null,ex.getMessage().toString(),"Client", JOptionPane.INFORMATION_MESSAGE);
                }
                catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(null,"Veuillez saisir correctement"
                                                                                    + "\nle numéro de la rue"
                                                                                    + "\n Le chiffre d'affaire"
                                                                                    + "\nLe nombre d'employé", "Client ", JOptionPane.INFORMATION_MESSAGE);
                }
                catch(Exception e){
                    System.out.println(e.toString());
                }
    }//GEN-LAST:event_buttonValide2ActionPerformed

    private void textFieldId2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldId2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldId2ActionPerformed
    
   
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EcfjFrame2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EcfjFrame2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EcfjFrame2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EcfjFrame2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EcfjFrame2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonQuitter2;
    private javax.swing.JButton buttonRetourAcceuil2;
    private javax.swing.JButton buttonValide2;
    private javax.swing.JComboBox<String> comboBoxDomaine2;
    private javax.swing.JComboBox<String> comboBoxProspectionInteresse2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelAdresse2;
    private javax.swing.JLabel labelChiffreDaffaire2;
    private javax.swing.JLabel labelCodePostal2;
    private javax.swing.JLabel labelCommentaire2;
    private javax.swing.JLabel labelDateProspection2;
    private javax.swing.JLabel labelDomaine2;
    private javax.swing.JLabel labelId2;
    private javax.swing.JLabel labelMail2;
    private javax.swing.JLabel labelNomRue2;
    private javax.swing.JLabel labelNombreDemployes2;
    private javax.swing.JLabel labelNumeroRue2;
    private javax.swing.JLabel labelProspectionInteresse2;
    private javax.swing.JLabel labelRaisonSocial2;
    private javax.swing.JLabel labelSousTitre2;
    private javax.swing.JLabel labelTelephone2;
    private javax.swing.JLabel labelTitre2;
    private javax.swing.JLabel labelVille2;
    private javax.swing.JPanel panelVariableClients2;
    private javax.swing.JPanel panelVariableProspect2;
    private javax.swing.JPanel panelVariableSociete2;
    private javax.swing.JTextArea textAreaCommentaire2;
    private javax.swing.JTextField textFieldChiffreDaffaire2;
    private javax.swing.JTextField textFieldCodePostal2;
    private javax.swing.JTextField textFieldDateProspection2;
    private javax.swing.JTextField textFieldDomaine2;
    private javax.swing.JTextField textFieldId2;
    private javax.swing.JTextField textFieldMail2;
    private javax.swing.JTextField textFieldNomRue2;
    private javax.swing.JTextField textFieldNombreDemployes2;
    private javax.swing.JTextField textFieldNumeroRue2;
    private javax.swing.JTextField textFieldRaisonSocial2;
    private javax.swing.JTextField textFieldTelephone2;
    private javax.swing.JTextField textFieldVille2;
    // End of variables declaration//GEN-END:variables
}
