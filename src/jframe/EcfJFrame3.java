/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jframe;

import classe.metier.Client;
import classe.metier.Prospect;
import jframe.EcfJFrame1.choixFormulaire;

/**
 *Fenetre pour afficher le tableau avec les détails des clients ou des prospects
 * @author cda 24
 */
public class EcfJFrame3 extends javax.swing.JFrame {

    
    // Constructeur------------------------------------------------------------- 

    /**
     * Constructeur avec choix du tableau
     * @param choix  type int
     */
    public EcfJFrame3(choixFormulaire choixFormulaire) {
        initComponents();
        
        switch(choixFormulaire){
            
            case AFFICHAGE_LISTE_CLIENT:
                initFormulaireAffichageClient();
                break;
                
            case AFFICHAGE_LISTE_PROSPECT:
                initFormulaireAffichageProspect();
                break;
                
               
        }
    }
    
    
    
    
    
    /**
     * Creates new form EcfJFrame3
     */
    public EcfJFrame3() {
        initComponents();
    }

    
    // Methode d'instance-------------------------------------------------------
    
    protected void initFormulaireAffichageClient(){
        
        jLabelTitre3.setText("Client");
        
        String entete []={"Raison Social", "Domaine", "Numéros de la rue", "Nom de la rue", "Code Postal","Ville",
                    "Telephone", "Mail de contact","Commentaire","Chiffre d'affaire","Nombre d'employés","Id"};
        
        int nbrLigne = Client.getListeClient().size();
        Object donnee[][]=new Object[nbrLigne][12];
        
        for (int i = 0; i < nbrLigne; i++) {
            
            donnee[i][0] = Client.getListeClient().get(i).getRaisonSocialeSociete();
            donnee[i][1] = Client.getListeClient().get(i).getDomaineSociete();
            donnee[i][2] = Client.getListeClient().get(i).getNumeroRueSociete();
            donnee[i][3] = Client.getListeClient().get(i).getNomRueSociete();
            donnee[i][4] = Client.getListeClient().get(i).getCodePostalSociete();
            donnee[i][5] = Client.getListeClient().get(i).getVilleSociete();
            donnee[i][6] = Client.getListeClient().get(i).getTelephoneSociete();
            donnee[i][7] = Client.getListeClient().get(i).getMailSociete();
            donnee[i][8] = Client.getListeClient().get(i).getCommentairesSociete();
            donnee[i][9] = Client.getListeClient().get(i).getChiffreDaffaireClient();
            donnee[i][10] = Client.getListeClient().get(i).getNombreEmployesClient();
            donnee[i][11] = Client.getListeClient().get(i).getIdSociete();
           
        }
      
        // Affectation d'un modèle pour le tableau client    
        jTableAffichage3.setModel(new javax.swing.table.DefaultTableModel(donnee,entete));
         
    }
    
    protected void initFormulaireAffichageProspect(){
        
        jLabelTitre3.setText("Prospect");
        
        String[] entetes = {"Raison Social", "Domaine", "Numéros de la rue", "Nom de la rue", "Code Postal","Ville","Telephone",
                            "Mail de contact","Commentaire","Date de prospecttion","Prospection interessé","Id"};
         
        int nbrLignes = Prospect.getListeProspect().size();
        Object donnee[][]=new Object[nbrLignes][12];
        
        for (int i = 0; i < nbrLignes; i++) {
            donnee[i][0] = Prospect.getListeProspect().get(i).getRaisonSocialeSociete();
            donnee[i][1] = Prospect.getListeProspect().get(i).getDomaineSociete();
            donnee[i][2] = Prospect.getListeProspect().get(i).getNumeroRueSociete();
            donnee[i][3] = Prospect.getListeProspect().get(i).getNomRueSociete();
            donnee[i][4] = Prospect.getListeProspect().get(i).getCodePostalSociete();
            donnee[i][5] = Prospect.getListeProspect().get(i).getVilleSociete();
            donnee[i][6] = Prospect.getListeProspect().get(i).getTelephoneSociete();
            donnee[i][7] = Prospect.getListeProspect().get(i).getMailSociete();
            donnee[i][8] = Prospect.getListeProspect().get(i).getCommentairesSociete();
            donnee[i][9] = Prospect.getListeProspect().get(i).getDateProspectionProspect();
            donnee[i][10] = Prospect.getListeProspect().get(i).getProspectInteresseProspect();
            donnee[i][11] = Prospect.getListeProspect().get(i).getIdSociete();
            
        }
        
            // Affectation d'un modèle pour le tableau prospect
            jTableAffichage3.setModel(new javax.swing.table.DefaultTableModel(donnee,entetes));
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelTitre3 = new javax.swing.JLabel();
        jLabelSousTitre3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableAffichage3 = new javax.swing.JTable();
        buttonRetourAcceuil3 = new javax.swing.JButton();
        buttonQuitter2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabelTitre3.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        jLabelTitre3.setText("jLabel");

        jLabelSousTitre3.setText("Affichage");

        jTableAffichage3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Titre 5", "Titre 6", "Titre 7", "Titre 8", "Titre 9", "Titre 10", "Titre 11", "Titre 12"
            }
        ));
        jScrollPane1.setViewportView(jTableAffichage3);

        buttonRetourAcceuil3.setText("Retour Acceuil");
        buttonRetourAcceuil3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRetourAcceuil3ActionPerformed(evt);
            }
        });

        buttonQuitter2.setText("Quitter");
        buttonQuitter2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonQuitter2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(241, 241, 241)
                        .addComponent(jLabelTitre3, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(149, 149, 149)
                        .addComponent(jLabelSousTitre3, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(314, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(buttonQuitter2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonRetourAcceuil3, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(129, 129, 129))
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabelTitre3, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(jLabelSousTitre3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 407, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonQuitter2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonRetourAcceuil3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonRetourAcceuil3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRetourAcceuil3ActionPerformed
       
        EcfJFrame1 ecfJFrame1 = new EcfJFrame1();
        ecfJFrame1.setTitle("Fenetre1");
        ecfJFrame1.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_buttonRetourAcceuil3ActionPerformed

    private void buttonQuitter2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonQuitter2ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_buttonQuitter2ActionPerformed
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EcfJFrame3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EcfJFrame3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EcfJFrame3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EcfJFrame3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EcfJFrame3().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonQuitter2;
    private javax.swing.JButton buttonRetourAcceuil3;
    private javax.swing.JLabel jLabelSousTitre3;
    private javax.swing.JLabel jLabelTitre3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableAffichage3;
    // End of variables declaration//GEN-END:variables
}
