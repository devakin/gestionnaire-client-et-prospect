/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classe.metier;

import exception.metier.personnalise.AttributSocieteInvalide;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *Classe métier client qui hérite de la classe mère Societe 
 * @author cda 24
 */
public class Client extends Societe{

    
    
    // Attribut Static----------------------------------------------------------
    
    private static ArrayList<Client> listeClient = new ArrayList<Client>();     //Liste de tous mes clients
    
    // Trie par rapport aux raison social et par ordre alphabetique 
    public static Comparator<Client> comparateurRaisonSocial = new Comparator<Client>(){
        @Override
        public int compare(Client c1, Client c2){
            return (c1.getRaisonSocialeSociete().compareToIgnoreCase(c2.getRaisonSocialeSociete()));
        }
    };
    
    
    // Getter et Setter Static--------------------------------------------------
    
    /**
     * Lire listeClient et faire trier la liste à chaque lecture
     * @return type ArrayList
     */
    public static ArrayList<Client> getListeClient() {
        Collections.sort(listeClient, comparateurRaisonSocial);
        return listeClient;
    }

    /**
     * Attribuer une nouvelle Arraylist dans listeClient
     * @param listeClient type ArrayList
     */
    public static void setListeClient(ArrayList<Client> listeClient) {
        Client.listeClient = listeClient;
    }
    
    
    // Methode Static-----------------------------------------------------------
    
    /**
     * Initialisation de 4 Clients pour test
     * @throws AttributSocieteInvalide s
     */
    public static void initClients() throws AttributSocieteInvalide{
        
        Client client1 = new Client();
        Client client2 = new Client();
        Client client3 = new Client();
        Client client4 = new Client();
        
        client1.setIdSociete(Societe.getCompteurIdSociete());
        client1.setRaisonSocialeSociete("SIEMENS");
        client1.setDomaineSociete(DomaineSociete_Enum.PRIVE);
        client1.setNumeroRueSociete(1);
        client1.setNomRueSociete("Allee Adrienne Lecouvreur");
        client1.setCodePostalSociete("75000");
        client1.setVilleSociete("Paris");
        client1.setTelephoneSociete("0987654321");
        client1.setMailSociete("contact@siemens.com");
        client1.setCommentairesSociete("En croissance");
        client1.setChiffreDaffaireClient(100000000);
        client1.setNombreEmployesClient(100);
        
        client2.setIdSociete(Societe.getCompteurIdSociete());
        client2.setRaisonSocialeSociete("LU SAS");
        client2.setDomaineSociete(DomaineSociete_Enum.PRIVE);
        client2.setNumeroRueSociete(2);
        client2.setNomRueSociete("Allee Adrienne Lecouvreur");
        client2.setCodePostalSociete("75000");
        client2.setVilleSociete("Paris");
        client2.setTelephoneSociete("0987654321");
        client2.setMailSociete("contact@lusas.com");
        client2.setCommentairesSociete("Ouverture nouvelle usine");
        client2.setChiffreDaffaireClient(200000000);
        client2.setNombreEmployesClient(2000);
        
        client3.setIdSociete(Societe.getCompteurIdSociete());
        client3.setRaisonSocialeSociete("MAIRIE DE METZ");
        client3.setDomaineSociete(DomaineSociete_Enum.PUBLIC);
        client3.setNumeroRueSociete(1);
        client3.setNomRueSociete("Place dArmes");
        client3.setCodePostalSociete("57000");
        client3.setVilleSociete("Metz");
        client3.setTelephoneSociete("0987654321");
        client3.setMailSociete("contact@mairiemetz.fr");
        client3.setCommentairesSociete("Appel d'offre bientot");
        client3.setChiffreDaffaireClient(2000000);
        client3.setNombreEmployesClient(500);
        
        client4.setIdSociete(Societe.getCompteurIdSociete());
        client4.setRaisonSocialeSociete("SNCF");
        client4.setDomaineSociete(DomaineSociete_Enum.PUBLIC);
        client4.setNumeroRueSociete(123);
        client4.setNomRueSociete("Place dArmes");
        client4.setCodePostalSociete("75002");
        client4.setVilleSociete("Paris");
        client4.setTelephoneSociete("0987654321");
        client4.setMailSociete("contact@sncf.fr");
        client4.setCommentairesSociete("A recontacter");
        client4.setChiffreDaffaireClient(200000000);
        client4.setNombreEmployesClient(5000);
        
        getListeClient().add(client1);
        getListeClient().add(client2);
        getListeClient().add(client3);
        getListeClient().add(client4);
        
    }
    
    
    // Attribut d'instance------------------------------------------------------
    
    private int chiffreDaffaireClient = 10;         // Chiffre d'affaire client
    private int nombreEmployesClient = 1;           // Nombre d'employé
    
    
    // Getter et Setter d'instance----------------------------------------------

    /**
     * Lire Chiffre d'affaire
     * @return the chiffreDaffaireClient type int
     */
    public int getChiffreDaffaireClient() {
        return chiffreDaffaireClient;
    }

    /**
     * Ecrire le chiffre d'affaire 
     * @param chiffreDaffaireClient type int 
     * @throws AttributSocieteInvalide
     * Erreur si chiffre d'affaire inférieur à 10
     */
    public void setChiffreDaffaireClient(int chiffreDaffaireClient) throws AttributSocieteInvalide {
       if(chiffreDaffaireClient < 10){
           throw new AttributSocieteInvalide("Le chiffre d'affaire ne doit pas etre inférieur à 10");
       }
       this.chiffreDaffaireClient = chiffreDaffaireClient;
    }

    /**
     * Lire le nombre d'employé
     * @return the nombreEmployesClient type int
     */
    public int getNombreEmployesClient() {
        return nombreEmployesClient;
    }
    
    /**
      * Ecrire le nombre d'employé
      * @param nombreEmployesClient type int 
      * @throws AttributSocieteInvalide 
      * Erreur si le champs de saisie est vide
      * Erreur si nombre d'employé est inférieur ou égale à zéro
      */
     public void setNombreEmployesClient(int nombreEmployesClient) throws AttributSocieteInvalide {
        
        if(nombreEmployesClient == 0){
            throw new AttributSocieteInvalide("Le nombre d'employé doit etre saisie");
        }
        else if(nombreEmployesClient <= 0){
            throw new AttributSocieteInvalide("Le nombre d'employé doit etre supérieur à 0 ");
        }       
        this.nombreEmployesClient = nombreEmployesClient;
    }

    
    // Constructeur------------------------------------------------------------- 

    /**
     * Constructeur Client sans les attributs
     */
    public Client() {
    }
    
     
    // Methode d'instance ------------------------------------------------------
    
    /**
     * Méthode pour vérifier (chiffre d'affaire / nombre d'employé)
     * @param chiffreDaffaire type int 
     * @param nombreDemploye type int
     * @throws AttributSocieteInvalide 
     * Erreur si (chiffre d'affaire / nombre d'employé) inférieur à 10
     */
    public void verificationChiffreDaffaireVsNombreDemployé(int chiffreDaffaire, int nombreDemploye) throws AttributSocieteInvalide{
        if ((float)(chiffreDaffaire/nombreDemploye)< 10){
            throw new AttributSocieteInvalide("Le (chiffre d'affaire / nombre d'employé) doit pas être strictement inférieur à 10 "); 
        }
    }

    
    // Le toString--------------------------------------------------------------

    /**
     * Affiche uniquement la raison sociale
     * @return type String
     */
    @Override
    public String toString() {
        //On peut aussi utiliser cette ligne pour afficher tous les attributs
        //return super.toString() + "\nChiffre d'affaire: " + chiffreDaffaireClient + "\nNombre d'employés: " + nombreEmployesClient;
        return this.getRaisonSocialeSociete();
    }
    
    
    
    
    
    
}
