/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classe.metier;

import exception.metier.personnalise.AttributSocieteInvalide;

/**
 *Classe mère des classes Client et Prospect
 * @author cda 24
 */
public abstract class Societe {
    
    
    // Attribut Static----------------------------------------------------------
    
    private static int compteurIdSociete = 1;       //Compteur Id
    
    
    // Getter et Setter Static--------------------------------------------------
    
    /**
     * Lire Compteur Id
     * @return the compteurIdSociete type int
     */
    public static int getCompteurIdSociete() {
        return compteurIdSociete;
    }

    /**
     * Ecrire Compteur Id
     * @param aCompteurIdSociete the compteurIdSociete to set type int
     */
    public static void setCompteurIdSociete(int aCompteurIdSociete) {
        compteurIdSociete = aCompteurIdSociete;
    }
   
    
    // Attribut d'instance------------------------------------------------------
    
    private int idSociete;                          //Id unique de la societe
    private String raisonSocialeSociete;            //La raison social de la societe
    private DomaineSociete_Enum domaineSociete;     //Domaine de la societe (PRIVE ou PUBLIQUE)
                                                    //Adresse de la societe
    private int numeroRueSociete;                   //Numero de la rue 
    private String nomRueSociete;                   //Nom de la rue 
    private String codePostalSociete;               //Code postale
    private String villeSociete;                    //Ville
    private String telephoneSociete;                //Numéro de téléphone de la societe
    private String mailSociete;                     //Email de la societe
    private String commentairesSociete;             //Commentaire de la societe

    
    // Getter et Setter d'instance----------------------------------------------

    

    /**
     * Lire Id unique de la societe
     * @return the idSociete type int
     */
    public int getIdSociete() {
        return idSociete;
    }

    /**
     * Ecrire Id unique de la societe
     * Et incrémente le compteur Id de 1 à chaque fois
     * @param idSociete the idSociete to set type int
     */
    public void setIdSociete(int idSociete) {
        this.idSociete = idSociete;
        compteurIdSociete++;
    }

    /**
     * Lire la raison social de la societe
     * @return the raisonSocialeSociete type String
     */
    public String getRaisonSocialeSociete() {
        return raisonSocialeSociete;
    }

    /**
     * Ecrire la raison social de la societe
     * @param raisonSocialeSociete the raisonSocialeSociete to set type String
     * @throws exception.metier.personnalise.AttributSocieteInvalide
     * Erreur si le champs de saisie est vide
     * Erreur si le saisie ne commence pas par un caractère
     */
    public void setRaisonSocialeSociete(String raisonSocialeSociete) throws AttributSocieteInvalide {
        if(raisonSocialeSociete.equals("")){
            throw new AttributSocieteInvalide("La raison social ne doit pas etre vide");
        }
        else if(!raisonSocialeSociete.matches("[\\w]+[\\w ]*")){
            throw new AttributSocieteInvalide("La raison social doit commencer par un caractère");
        }    
        this.raisonSocialeSociete = raisonSocialeSociete;
    }

    /**
     * Lire le domaine de la societe
     * @return the domaineSociete type Enum
     */
    public DomaineSociete_Enum getDomaineSociete() {
        return domaineSociete;
    }

    /**
     * Ecrire le domaine de la societe
     * @param domaineSociete the domaineSociete to set type Enum
     */
    public void setDomaineSociete(DomaineSociete_Enum domaineSociete)  {
        this.domaineSociete = domaineSociete;
    }

    /**
     * Lire le numero de la rue
     * @return the numeroRueSociete type int 
     */
    public int getNumeroRueSociete() {
        return numeroRueSociete;
    }

    /**
     * Ecrire le numero de la rue
     * @param numeroRueSociete the numeroRueSociete to set type int
     * @throws exception.metier.personnalise.AttributSocieteInvalide
     * Erreur si le champs de saisie est vide
     * Erreur si le numero de la rue est négatif
     */
    public void setNumeroRueSociete(int numeroRueSociete) throws AttributSocieteInvalide {
        if(numeroRueSociete == 0){
            throw new AttributSocieteInvalide("Le numéro de la voie doit etre saisie");
        }
        else if(numeroRueSociete < 0){
            throw new AttributSocieteInvalide("Le numéro de la voie doit etre positif");
        }    
        this.numeroRueSociete = numeroRueSociete;
    }

    /**
     * Lire le nom de la rue 
     * @return the nomRueSociete type String
     */
    public String getNomRueSociete() {
        return nomRueSociete;
    }

    /**
     * Ecire le nom de la rue 
     * @param nomRueSociete the nomRueSociete to set type String 
     * @throws exception.metier.personnalise.AttributSocieteInvalide
     * Erreur si le champs de saisie est vide
     * Erreur si le saisie ne commence pas par un caractère
     */
    public void setNomRueSociete(String nomRueSociete) throws AttributSocieteInvalide {
        if(nomRueSociete.equals("")){
            throw new AttributSocieteInvalide("Le nom de la rue doit etre saisie");
        }
        else if(!nomRueSociete.matches("[\\w]+[\\w ]*")){
            throw new AttributSocieteInvalide("Le nom de la rue doit commencer par un caractère");
        } 
        this.nomRueSociete = nomRueSociete;
    }

    /**
     * Lire le code postal
     * @return the codePostalSociete type String
     */
    public String getCodePostalSociete() {
        return codePostalSociete;
    }

    /**
     * Ecrire le code postal 
     * @param codePostalSociete the codePostalSociete to set type String
     * @throws exception.metier.personnalise.AttributSocieteInvalide
     * Erreur si le champs de saisie est vide
     * Erreur si le saisie n'est pas au format ex:01000
     */
    public void setCodePostalSociete(String codePostalSociete) throws AttributSocieteInvalide {
        if(codePostalSociete.equals("")){
            throw new AttributSocieteInvalide("Le code postal doit etre saisie");
        }
        else if(!codePostalSociete.matches("[0-9]{5}")){
            throw new AttributSocieteInvalide("Le code postal doit etre de format ex: 01000");
        } 
        this.codePostalSociete = codePostalSociete;
    }

    /**
     * Lire la ville
     * @return the villeSociete type String
     */
    public String getVilleSociete() {
        return villeSociete;
    }

    /**
     * Ecrire la ville 
     * @param villeSociete the villeSociete to set type String 
     * @throws exception.metier.personnalise.AttributSocieteInvalide
     * Erreur si le champs de saisie est vide
     * Erreur si le saisie ne commence pas par un caractère
     */
    public void setVilleSociete(String villeSociete) throws AttributSocieteInvalide {
        if(villeSociete.equals("")){
            throw new AttributSocieteInvalide("La ville doit etre saisie");
        }
        else if(!villeSociete.matches("[\\w]+[\\w -]*")){
            throw new AttributSocieteInvalide("La ville doit commencer par un caractère");
        } 
        this.villeSociete = villeSociete;
    }

    /**
     * Lire le numéro de téléphone 
     * @return the telephoneSociete type String 
     */
    public String getTelephoneSociete() {
        return telephoneSociete;
    }

    /**
     * Ecrire le numéro de téléphone
     * @param telephoneSociete the telephoneSociete to set type String
     * @throws exception.metier.personnalise.AttributSocieteInvalide
     * Erreur si le champs de saisie est vide
     * Erreur si le numéro de téléphone ne correspond pas aux format national ou international
     */
    public void setTelephoneSociete(String telephoneSociete) throws AttributSocieteInvalide {
        if(telephoneSociete.equals("")){
            throw new AttributSocieteInvalide("Le numéro de téléphone doit etre saisie");
        }
        else if(!telephoneSociete.matches("[+][0-9]{11}|[0-9]{10}")){
            throw new AttributSocieteInvalide("Le numéro de téléphone doit etre aux format national ou international");
        } 
        this.telephoneSociete = telephoneSociete;
    }

    /**
     * Lire l'adresse mail
     * @return the mailSociete type String
     */
    public String getMailSociete() {
        return mailSociete;
    }

    /**
     * Ecrire l'adresse mail
     * @param mailSociete the mailSociete to set type String
     * @throws exception.metier.personnalise.AttributSocieteInvalide
     * Erreur si le champs de saisie est vide
     * Erreur si l'addresse mail ne correspond pas à un formail email
     */
    public void setMailSociete(String mailSociete) throws AttributSocieteInvalide {
        if(mailSociete.equals("")){
            throw new AttributSocieteInvalide("L'adresse mail doit etre saisie");
        }
        else if(!mailSociete.matches("^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$")){
            throw new AttributSocieteInvalide("L'adresse mail doit etre au bon format");
        } 
        this.mailSociete = mailSociete;
    }

    /**
     * Lire un commentaire
     * @return the commentairesSociete type String
     */
    public String getCommentairesSociete() {
        return commentairesSociete;
    }

    /**
     * Ecrire un commentaire 
     * @param commentairesSociete the commentairesSociete to set type Sting
     */
    public void setCommentairesSociete(String commentairesSociete) {
        this.commentairesSociete = commentairesSociete;
    }
    
    
    // Constructeur------------------------------------------------------------- 

    /**
     * Constructeur Societe sans les attributs
     */
    public Societe() {   
    }
    
////    /**
////     * Constructeur Societe avec tous les attributs
////     * @param raisonSocialeSociete
////     * @param domaineSociete
////     * @param numeroRueSociete
////     * @param nomRueSociete
////     * @param codePostalSociete
////     * @param villeSociete
////     * @param telephoneSociete
////     * @param mailSociete
////     * @throws AttributSocieteInvalide 
////     */
////    public Societe(String raisonSocialeSociete, DomaineSociete_Enum domaineSociete, 
////            int numeroRueSociete, String nomRueSociete, String codePostalSociete, 
////            String villeSociete, String telephoneSociete, String mailSociete) throws AttributSocieteInvalide {
////        
////        this.setRaisonSocialeSociete(raisonSocialeSociete);
////        this.setDomaineSociete(domaineSociete);
////        this.setNumeroRueSociete(numeroRueSociete);
////        this.setNomRueSociete(nomRueSociete);
////        this.setCodePostalSociete(codePostalSociete);
////        this.setVilleSociete(villeSociete);
////        this.setTelephoneSociete(telephoneSociete);
////        this.setMailSociete(mailSociete);
////    }
    
    
    // Methode d'instance-------------------------------------------------------
    
    /**
     * Enum domaine societe PRIVE OU PUBLIQUE
     */
    public enum DomaineSociete_Enum {   
    PRIVE,PUBLIC;
    }
    
    // Le toString--------------------------------------------------------------

    /**
     * Liste de tous les attributs en String
     * @return type String
     */
    @Override
    public String toString() {
        return "ID: " + getIdSociete() +
                "\nRaison Sociale: " + getRaisonSocialeSociete() + 
                "\nDomaine: " + getDomaineSociete() + 
                "\nAdresse Numero de la rue: " + getNumeroRueSociete() + 
                "\nNom de la voie: " + getNomRueSociete() + 
                "\nCode postal: " + getCodePostalSociete() + 
                "\nVille: " + getVilleSociete() + 
                "\nTelephone: " + getTelephoneSociete() + 
                "\nMail: " + getMailSociete() + 
                "\nCommentaires: " + getCommentairesSociete();
    }


    
}
